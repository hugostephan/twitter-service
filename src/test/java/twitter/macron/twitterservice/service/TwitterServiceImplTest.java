package twitter.macron.twitterservice.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import twitter.macron.twitterservice.bean.Tweet;
import twitter.macron.twitterservice.repository.TweetRepository;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TwitterServiceImplTest {
    // Mockito Config
    @Mock
    TweetRepository tweetRepository ;
    @InjectMocks
   TwitterServiceImpl twitterService = new TwitterServiceImpl();

    ArrayList<Tweet> listTweet ;

    /**
     * Set up mokito annotations and create a Club useful for testing
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        this.initData();
        this.initMockito();
    }

    private void initData() throws Exception {
        Tweet t = new Tweet();
        t.setDate(new Date());
        t.setId(1);
        t.setMessage("message");
        t.setUsername("username");

        this.listTweet = new ArrayList<>();
        this.listTweet.add(t);
    }

    private void initMockito() throws Exception {
        // init annotations
        MockitoAnnotations.initMocks(this);

        // stubbing
        Mockito.when(this.tweetRepository.findAllByOrderByDateDesc()).thenReturn(this.listTweet);
        Mockito.when(this.tweetRepository.findByMessageContaining("message")).thenReturn(this.listTweet);
    }

    @Test
    public void getAllTweets() throws Exception {
        // test
        assertEquals(this.listTweet, twitterService.getAllTweets());

        // verify
        Mockito.verify(this.tweetRepository, times(1)).findAllByOrderByDateDesc();
    }

    @Test
    public void searchTweets() throws Exception {
        // test
        assertEquals(this.listTweet, twitterService.searchTweets("message"));

        // verify
        Mockito.verify(this.tweetRepository, times(1)).findByMessageContaining("message");
    }

}