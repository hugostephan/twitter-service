package twitter.macron.twitterservice.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import twitter.macron.twitterservice.bean.User;
import twitter.macron.twitterservice.repository.UserRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

@RunWith(JUnitParamsRunner.class)
public class authServiceImplTest {
    @InjectMocks
    authServiceImpl service;
    @Mock
    UserRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Parameters({"macron_Mister_Univers@gmail.com","brigitte_Miss_Univers@gmail.com"} )
    public void findByUsername(String mail) throws Exception {
        // data init
        User u = new User();
        u.setUsername(mail);

        // stubbing
        Mockito.when(repository.findByUsername(mail)).thenReturn(u);

        // test
        assertEquals(u, service.findByUsername(mail));

        // verify
        Mockito.verify(this.repository, times(1)).findByUsername(mail);
    }
}
