package twitter.macron.twitterservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TwitterServiceApplication : Main Spring Boot Program entry point
 */
@SpringBootApplication
public class TwitterServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterServiceApplication.class, args);
	}
}
