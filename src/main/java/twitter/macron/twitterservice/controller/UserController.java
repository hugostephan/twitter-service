package twitter.macron.twitterservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * UserController handles main web mapping
 */
@Controller
public class UserController {

    @Autowired
    twitter.macron.twitterservice.service.authService service;


    /**
     * Show connection form
     * @return view name for thymeleaf engine
     */
    @RequestMapping(value = {"/login"})
    String index() {
        return "login";
    }

    /**
     * Login page, all unauthentified users are redirected to this page
     * @param model Persistence Data Model
     * @param error Potential login error
     * @param logout Does the user just logout ?
     * @return
     */

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and/or password are invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
}
