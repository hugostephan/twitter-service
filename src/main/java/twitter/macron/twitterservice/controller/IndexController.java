package twitter.macron.twitterservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import twitter.macron.twitterservice.bean.Tweet;
import twitter.macron.twitterservice.service.TwitterService;

import java.util.List;

/**
 * IndexController handles main web mapping
 */
@Controller
public class IndexController {
    @Autowired
    TwitterService twitterService;

    /**
     * Show list tweet view
     * @param model Model Attribute for persistent data
     * @return view name for thymeleaf engine
     */
    @RequestMapping(value = {  "/",  "/index"})
    String index(Model model)  {
        List<Tweet> listTweet = twitterService.getAllTweets();
        model.addAttribute("listtweet",listTweet);
        model.addAttribute("message",new String());
        return "index";
    }

    /**
     * Show found tweets view for a given message
     * @param message Message to search for
     * @param model Model Attribute for persistent data
     * @return view name for thymeleaf engine
     */
    @RequestMapping(value = { "/search"} , method = RequestMethod.POST)
        public String search(@ModelAttribute("message") String message, Model model) {
        List<Tweet> listTweet = twitterService.searchTweets(message);
        model.addAttribute("listtweet", listTweet);
        model.addAttribute("message", message);
        return "index";
    }

}
