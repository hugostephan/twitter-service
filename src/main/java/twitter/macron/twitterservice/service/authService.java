package twitter.macron.twitterservice.service;


import twitter.macron.twitterservice.bean.User;


/**
 * TwitterUser provides interface to manipulate Users from DB
 */

public interface authService {

    /**
     * Get user by username from DB
     * @return User
     */
    User findByUsername(String username);

    /**
     * Save User on DB
     * @return User
     */
    User saveUser(User user);
}
