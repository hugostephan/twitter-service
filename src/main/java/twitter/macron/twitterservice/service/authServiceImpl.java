package twitter.macron.twitterservice.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import twitter.macron.twitterservice.bean.User;
import twitter.macron.twitterservice.repository.UserRepository;


/**
 * TwitterUser Implementation
 */

@Service
public class authServiceImpl implements authService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Function which find User by her UserName
     * @return tweet list
     */
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Save User calling by PostConstruct method
     * @return User
     */
    @Override
    public User saveUser(User user) {
        if (user.getUsername() == null) {
            return null;
        }
        if ((user.getPassword() != null)) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        } else if (this.findByUsername(user.getUsername()) != null) {
            user.setPassword(this.findByUsername(user.getUsername()).getPassword());
        } else {
            return null;
        }

        User user1 = userRepository.save(user);
        return user1;
    }
}
