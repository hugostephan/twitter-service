package twitter.macron.twitterservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter.macron.twitterservice.bean.Tweet;
import twitter.macron.twitterservice.repository.TweetRepository;

import java.util.List;

/**
 * TwitterService Implementation
 */
@Service
public class TwitterServiceImpl implements TwitterService {
    @Autowired
    TweetRepository tweetRepository;
    /**
     * Get All Tweets
     * @return
     */
    @Override
    public List<Tweet> getAllTweets() {
        return tweetRepository.findAllByOrderByDateDesc();
    }

    /**
     * Get Tweets From DB
     * @Param : message to search
     * @return
     */
    @Override
    public List<Tweet> searchTweets(String message){
        return tweetRepository.findByMessageContaining(message);
    }
}
