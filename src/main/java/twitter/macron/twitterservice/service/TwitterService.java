package twitter.macron.twitterservice.service;

import java.util.List;
import twitter.macron.twitterservice.bean.Tweet;
import java.util.List;

/**
 * TwitterService provides interface to manipulate tweets from DB
 */
public interface TwitterService {
    /**
     * Get All Tweets From DB
     * @return
     */
    List<Tweet> getAllTweets();

    /**
     * Search for specific tweets according to givend String message
     * @param message message to search for
     * @return relevants tweets if found
     */
    List<Tweet> searchTweets(String message);
}