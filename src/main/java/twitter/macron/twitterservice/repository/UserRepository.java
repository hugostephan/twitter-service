package twitter.macron.twitterservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import twitter.macron.twitterservice.bean.User;


/**
 * UserRepository : extends JPA repo and provides classic DB queries.
 */

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    /**
     * Get User by UserName
     * @return User
     */
    User findByUsername(String username);
}
