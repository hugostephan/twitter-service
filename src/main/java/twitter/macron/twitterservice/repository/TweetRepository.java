package twitter.macron.twitterservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import twitter.macron.twitterservice.bean.Tweet;

import java.util.List;

/**
 * TweetRepository : extends JPA repo and provides classic DB queries.
 */
@Repository
public interface TweetRepository extends JpaRepository<Tweet,Integer> {
    /**
     * Fetch all tweet from DB and order by id desc
     * @return tweet list
     */
    List<Tweet> findAllByOrderByIdDesc();

    /**
     * Fetch all tweet from DB and order by date desc
     * @return tweet list
     */
    List<Tweet> findAllByOrderByDateDesc();

    /**
     * Fetch a tweet based on a message
     * @param message tweet text
     * @return Tweet object containing the given message if it exists in DB.
     */
    List<Tweet> findByMessageContaining(String message);
}
